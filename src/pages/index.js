"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var gatsby_1 = require("gatsby");
var styles = require("./Index.module.scss");
exports.indexPageQuery = gatsby_1.graphql(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n    query IndexPageQuery {\n        site{\n            siteMetadata {\n                siteUrl\n                title\n                description\n            }\n        }\n    }\n"], ["\n    query IndexPageQuery {\n        site{\n            siteMetadata {\n                siteUrl\n                title\n                description\n            }\n        }\n    }\n"])));
var IndexPage = /** @class */ (function (_super) {
    __extends(IndexPage, _super);
    function IndexPage() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    IndexPage.prototype.render = function () {
        return (React.createElement("div", { className: styles.Container },
            React.createElement("h1", null, " Title")));
    };
    return IndexPage;
}(React.Component));
exports.default = IndexPage;
var templateObject_1;
//# sourceMappingURL=index.js.map