import * as React from "react"

const PrevNext = (props:any) => {
    const { prev, next } = props
    return (    
        <ul>
            {next && <li><a href={next.fileds.slug}>Next {" "}
                {next.frontmatter.title} </a></li>}
            {prev && <li><a href={prev.fields.slug}>Prev {" "}
                {prev.frontmatter.title}</a></li>}
            
        </ul>    

    )
}
export default PrevNext;