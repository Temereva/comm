import * as React from "react"
import { Link } from "gatsby"
import * as PropTypes from "prop-types"



const PageNavigation = (props:any) => {
    const prevPost = props.prevLink;
    const prevName = props.prevTitle;
    const nextPost = props.nextLink;
    const nextName = props.nextTitle;
    const prev = props.prev.Obj;
    const next = props.nextObj;
    return (

        <div>
             <ul>
                <li>
                    {prev && (
                        <Link to={`/${prevPost}`} rel="prev">
                        ← {prevName}
                        </Link>
                    )}
         
                </li>
                <li>
                     {next && (
                        <Link to={`/${nextPost}`} rel="next">
                        ← {nextName}
                        </Link>
                    )}
                </li>
             </ul>
        </div>

    )
}

PageNavigation.propTypes = {
    prevPost : PropTypes.string,
    prevName: PropTypes.string,
    nextPost: PropTypes.string,
    nextName: PropTypes.string,
  }
  
PageNavigation.defaultProps = {
    prevPost : '',
    prevName: '',
    nextPost: '',
    nextName: '',
}
  
export default PageNavigation
