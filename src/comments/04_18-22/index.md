---
quote: "4_18-22"
chapter: "4"
verses: "18-22"
title: "От Матфея 4:18-22"
subtitle: "«Проходя же близ моря Галилейского, Он увидел двух братьев...»"
excerpt: "Проходя же близ моря Галилейского, Он увидел двух братьев: Симона, называемого Петром, и Андрея, брата его, закидывающих сети в море, ибо они были рыболовы"
date: "2009-06-20"
tags: ["Призвание Апостолов","Апостолы и добродетели"]
posttype: "comment"
---
![alt Доменико Гирландайо, Призвание апостолов Петра и Андрея.](/Domenico_Ghirlandaio_vocation_of_apostles.jpg "Доменико Гирландайо, Призвание апостолов Петра и Андрея, Сикстинская капелла, Ватикан, 1481-1482 гг.")

>18. Проходя же близ моря Галилейского, Он увидел двух братьев: Симона, называемого Петром, и Андрея, брата его, закидывающих сети в море, ибо они были рыболовы,
>19. и говорит им: идите за Мною, и Я сделаю вас ловцами человеков.
>20. И они тотчас, оставив сети, последовали за Ним.
>21. Оттуда, идя далее, увидел Он других двух братьев, Иакова Зеведеева и Иоанна, брата его, в лодке с Зеведеем, отцом их, починивающих сети свои, и призвал их.
>22. И они тотчас, оставив лодку и отца своего, последовали за Ним.

**Нирмала**: В этом эпизоде происходит призвание первых четырёх апостолов. Как мы все знаем, апостолы символизируют добродетели. То есть первые четыре добродетели, которые призывает Иисус, это Андрей - апостол Надежды, Пётр - апостол Веры, Иаков - апостол Мудрости и Иоанн - апостол Любви. Причем Вера и Надежда являются братьями, так же как Любовь и Мудрость. В этом я увидела параллель с четырьмя Йогами - карма-йога, раджа-йога, джняна-йога и бхакти-йога. Их на самом деле больше, но основных - именно четыре. Апостолов тоже больше, но именно этим четырём уделяется наибольшее внимание. Кроме того, как добродетели без Бога являются ничем, так же и Карма без Йоги - ничто, Раджа без Йоги - ничто, в общем, всё - ничто без Йоги. Здесь это наиболее видно: Андрея, который был учеником Иоанна Крестителя, я бы соотнесла с раджа-йогой [^1], Петра - с карма-йогой [^2], Иакова - с джняна-йогой [^3], а Иоанна - с бхакти-йогой [^4]. В дальнейшем будет видно, что Иисус будет уделять наибольшее внимание карма-йоге (Петру), и это указывает на то, что исполнение карма-йоги является наиболее доступным способом осуществления единения с Богом в нашу эпоху. Петра, как правило, будет везде сопровождать Иоанн, даже когда Пётр его и не зовёт, даже если он этим возмущается. Так Любовь, бхакти-йога всегда будет присутствовать в  практике карма-йоги.

Если смотреть с точки зрения физиологии, то апостолов можно соотнести с двенадцатью парами черепно-мозговых нервов, из которых четыре отвечают за чувства. На тот момент, когда эти четверо учеников ещё занимаются рыболовством, они являются управителями нижнего ментала, это то, о чём в молитве «Отче наш» сказано «и избави нас от лукавого». Их сознание находится  на уровне свадхистханы, на уровне животной природы человека. В тот же момент, когда они встречают Христа, происходит осознание их другой природы - Божественной. Они оставляют лодку свою (свадхистхана), поднимаясь на совершенно другой уровень.

Ещё я увидела здесь пратьяхару [^5], то есть полное отсечение чувств, полный контроль над ними - когда чувства, вместо того, чтобы привязывать человека к миру майи, начинают ему подчиняться. У меня всё.

**Рубик**: Кто ещё хочет сказать по поводу этого эпизода?

**Ананда**: Иоанн немного поправляет остальных евангелистов. У него есть эпизод описывающий призвание апостолов, который кажется совершенно непонятным незначительным:
>34. И я видел и засвидетельствовал, что Сей есть Сын Божий.
>35. На другой день опять стоял Иоанн и двое из учеников его.
>36. И, увидев идущего Иисуса, сказал: вот Агнец Божий.
>37. Услышав от него сии слова, оба ученика пошли за Иисусом.
>38. Иисус же, обратившись и увидев их идущих, говорит им: что вам надобно? Они сказали Ему: Равви́,- что значит: учитель,- где живешь?
>39. Говорит им: пойдите и увидите. Они пошли и увидели, где Он живет; и пробыли у Него день тот. Было около десятого часа.
>40. Один из двух, слышавших от Иоанна об Иисусе и последовавших за Ним, был Андрей, брат Симона Петра [Ин. 1:34-40].

То есть здесь такие подробности, которые кажутся лишними, как бы лишний эпизод. 

**Рубик**: Почему?

**Ананда**: Потому что это как бы защита знания от духа, который не готов, он видит это считает лишними подробностями, не важными и как бы опускает. Дух, который готов видит в этом знание. В таких эпизодах раскрывается тайная часть знаний. 

В первой части этого эпизода Иоанн хочет показать, что 40 дней искушения в пустыне не были сорока земными днями, и пустыня - это не земная пустыня.

**Рубик**: Какая добродетель соответствует апостолу Андрею?

**Нирмала**: Надежда.

**Рубик**: Надежда... Андрей... Так.

**Ананда**: А второй был, получается, Иаков?

**Рубик**:  А ему какая добродетель соответствует?

**Ананда**: Мудрость.

**Рубик**: Так. Разматываем этот клубочек...

**Ананда**: Первый узел - в том, что они называют Его Учителем. Их устами провозглашается Его статус Учителя.

**Рубик**: Сейчас на иврите говорят «раббай», а на самом деле «раббай» и «равви» - это не совсем одно и то же. Всего одна буква отличается, но это отличает просто раввина от того, кто проповедует в пустыне, как Иоанн Креститель. Само слово «равви», посмотрите, насколько оно ёмкое. Корень «ра» обозначает истинный свет, корень «ви» или «вита» - истинную жизнь, не животную, а высшую. Так Равви есть «освещающий жизнь», то есть тот, кто вносит в жизнь истинный свет, не «лю» (люмен, люцифер, лучезарный) а «ра» - «радость», «вивеку», то есть различение, дифференциацию, то, что позволяет отделить одно от другого, узреть разницу. Кроме того, здесь ещё присутствует «авва», что значит «отец» - отец всех, не мой лично. Назвав Его таким образом добровольно, они признали за Ним все высшие точки в себе, которые они отдали в руководство Ему. 

Дальше.

**Ананда**: «Где живёшь?»

**Рубик**: Гениально! Где он живет? Ниведита?

**Ниведита**: Может быть, аджна.

**Рубик**: То есть Он их пригласил в аджну?

**Ниведита**: Конечно, а куда ещё?

**Рубик**: Абсолютно правильно! Именно в аджну! Место пребывания Сына Божия  - это аджна, хотя между ними и нет разницы (вообще то, ни между кем нет разницы). А что такое аджна, Шанти?

**Шанти**: Свет, Любовь.

**Рубик**: «Под косой луна блестит, а во лбу звезда горит». Звезда, не звездочка. Как у нас классифицируется Солнце?

**Нирмала**: Желтый карлик.

**Рубик**: Вот! Смотрите, что Желтый карлик вытворяет, а что может сделать звезда помощнее?!

Я хочу, чтобы вы перестали заниматься земным бредом. Хотя бы сейчас. Был, к примеру, такой тупой-тупой фильм, какой-то парень все время гонялся за красивыми девушками, и однажды некий экстрасенс загипнотизировал его, сказав: «Теперь ты будешь видеть в людях их внутреннюю красоту». Вскоре тот влюбился в девушку, и ему казалось, что она - само совершенство, такая худенькая, изящная, но, когда она прыгала в бассейн, всех вокруг просто смывало! Он не понимал, почему. Когда его вывели из гипноза - прямо перед свадьбой - это был шок, он её не узнал, пришлось заново влюбляться [Фильм братьев Фарелли «Любовь зла» (Shallow Hal, 2001)]. Так вот, я хотел бы, чтобы вы судили об истине не только по тому, как вы её видите, но и по тем брызгам, которые от неё летят.  

Чем отличаются такие люди, как Анастасия [Героиня серии книг В. Мегрэ «Звенящие кедры России»]? У них времени много, они могут лежать на опушке леса и слушать, и смотреть, и делать выводы, вплоть до сверхфизических, то есть, когда открываются новые знания о явлениях  природы... Тот же Жан-Батист Ламарк - от скуки начал следить за растениями, животными, годами наблюдал за ними, смотрел, чем они отличаются друг от друга, какие есть в этих отличиях закономерности, как они связаны между собой и с условиями обитания, и эти исследования стали первой форточкой в эволюционную теорию Дарвина. А чем отличается 21 век? Ужасный век. В прошлые века они были бездельниками, а давать высокие знания бездельникам при довольно низком уровне сознания очень опасно - уничтожить всё могут. Сейчас уже не опасно, потому что, хоть осознанность в среднем и выше, и мозги уже неплохие, но жизнь, которую мы себе устроили, засасывает настолько, что даже на сон времени не хватает, даже на спокойное дыхание, не то что уж на созерцание.

Слава Богу, что вы сознательно взяли и определили время для садханы, внутри которой существует время для медитации. Вот, вы в своей медитации созерцаете некие сверхфизические процессы. А вы понимаете, что там происходит? Вы слышите? Видите? Вы понимаете показанное? Вы расставляете показанное на свои места? Где находится Солнце, Луна? Почему мы считаем, что Солнце больше Земли? Что мы берем за основу наших расчётов? 

Какие основные физические величины вам известны? Масса, расстояние, мера объёма, мера длины, время… Задайтесь вопросом: почему именно эти величины положены в основу понимания физических явлений? И правильно ли мы эти величины видим и понимаем? Объясните мне, почему масса измеряется в граммах и килограммах? Почему расстояние измеряется метрами? Кто мне попробует ответить на этот вопрос с психологической точки зрения? Это вопросы, которые вы должны были в 5 лет родителям задавать.

**Ниведита**: Рубик, ты нам говорил, что размер Земли на самом деле гораздо больше, чем официально считается. Получается, что само понятие о расстоянии не имеет смысла...

**Рубик**: Имеет. Пока вы находитесь в мире феноменов, у вас есть ответственность перед своим сознанием и перед сознанием окружающих. По мере того, как вы переходите на более и более высокие планы сознания, на вас возлагается всё большая ответственность в общении с низкими планами. Вы должны знать те приборы, которыми они пользуются и те результаты, которые они получают в результате измерения своими приборами. Познавая новое, вы обязаны знать старое. Осваивая стрельбу из пистолета, вы должны представлять себе, как люди пользовались каменными топорами, нападая друг на друга. Одно знание не прекращает другого, более того, оно его обусловливает и умножает.

**Ананда** Если представить миры в виде воронки: мир причин, мир идей и мир форм, то там, где узкое место, в мире форм - три измерения, в мире идей их четыре, пять, шесть или больше, а в мире причин, вообще сто. Если взять трехмерный объект, куб например, и рассмотреть его на плоскости, то мы видим только его проекцию. Это может быть всего лишь одна точка, на которой он стоит. Это касается всего, мы видим только какой-то срез реальных объектов, плюс наше сознание привязано ко времени, оно движется вдоль времени.

**Ананда**: Если представить миры в виде воронки: мир причин, мир идей и мир форм, то там, где узкое место, в мире форм, - три измерения, в мире идей их четыре, пять, шесть или больше, а в мире причин, вообще сто. Если взять трехмерный объект, куб например, и рассмотреть его на плоскости, то мы видим только его проекцию. Это может быть всего лишь одна точка, на которой он стоит. Это касается всего, мы видим только какой-то срез реальных объектов, плюс наше сознание привязано ко времени, оно движется вдоль времени.

**Рубик**: Что такое время, кстати?  Кала, Маха-Кала... Какое слово переводится на русский как «дарующий сознание времён»? 

**Ананда**: Кала-Дхара?

**Рубик**: Дар, Дарий, дарить - это на всех языках едино, и на русском в том числе. Дар и Кала, Кала-н-дар - календарь.

А что такое кала? Это бесконечно малая величина, которая проходит между самыми быстрыми явлениями. Калу можно рассчитать. Но опять же, что мы считаем самыми быстрыми явлениями? Скорость света? 

**Ниведита**: Скорость мысли?

**Рубик**: Скорость мысли - это хорошо. Это очень много! Представляешь себе, в каком состоянии должна быть мысль? Представляешь мощь этого инструмента, его созидающую и разрушающую способность? Представь, если это сознание чем-то уязвлено, поражено; сколько бед оно может причинить?

**Ниведита**: Всей вселенной?

**Рубик**: Всей вселенной! Ты не представляешь себе, ведь всю жизнь человечества, мы только и говорим о войнах. Почему? Потому что не случается такого момента, когда бы не проявлял себя какой-то дефект чьего-то сознания, от которого всю вселенную приходится спасать. Вселенную! Не звездочку, не шарик! От одного! А их - ионы, бесчисленное множество душ. А теперь представьте, сколько таких дефективных? Не с той ноги встал, кто-то на него косо посмотрел, немножко не под тем углом... Нормальный хороший человек, но что-то случилось. Он и не хотел, и не было злой воли… Я уж не говорю про отдельную статью «злая воля», где можно так навернуться, что мало не покажется. 

Если вы посмотрите в Святые Писания любых народов, вы не найдете одной минуты, одной секунды без упоминания войны. А что, это им так хочется? Нет, ничего подобного. А каждый из вас разве не воин, в некотором роде? Разве вы не являетесь стражниками и поборниками того бреда, который в вас возникает? Да, постоянно! Вы посмотрите, даже в обычных, нормальных семьях, где люди любят друг друга, они все равно искры друг в друга пускают. И мы говорим, что это нормально, и не то, что прощаем, а даже скучаем без этого. 

Вся жизнь мироздания - это война. Само тело человека - поле боя. Желудок воюет с двенадцатиперстной кишкой и с кровью, печень вообще со всем, чем угодно и так далее.. А когда прекратится эта война? Только когда вы обретете сознание высших духовных планов. Когда подниметесь на высшие астральные, несотворённые планеты. Когда вы придёте на ту планету, или звезду - не важно как это называется - где живёт Сам Господь Бог, либо когда вы измените своё сознание таким образом, чтобы проекция этих несотворённых планет, попала в то место, где находится ваша стопа.

<p class="alignCenter">***</p>

Итак, что такое метр и что такое грамм? Почему мы берем для измерения событий, происходящих вокруг нас, именно эти физические величины? Давайте ставить всё на свои места. Что такое расстояние? Расстояние - это «Чем дальше, тем хуже я тебя вижу»; «Чем дальше, тем хуже я тебя слышу»; «Чем дальше, тем меньше ты кажешься»; «Чем дальше, тем темнее». Так чем должно измеряться расстояние?

**Ниведита**: Освещенностью.

**Арджуна**: Человеческой восприимчивостью.

**Рубик**: Конечно! Умница! Это прежде всего! Не внешне-физическая величина, навязанная непонятно кем, а естественная величина, человеческая, вложенная в суть самого человека, исходящая из его абсолютности. Как можно её назвать - вариантов много, можно сказать и «восприимчивость», тут, правда, есть нюансы: одного воспринимаешь лучше, другого хуже. Двое стоят на одинаковом расстоянии от тебя, но тебе кажется, что один за миллиарды парсек, а другой - рядом.

Теперь, что такое масса? Фотон - самый маленький - имеет самый слабенький свет. Чем больше света, тем объект крупнее. Правильно? Таким образом массу правильнее измерять величиной света.

Дальше - время! Что мы берем за основу измерения времени? Минута, секунда, час… Что такое секунда, миллисекунда, микросекунда? Существует ли мельчайшая неделимая величина, меньше которой уже не может быть, в отношении дискретности времени? И дискретно ли время? И что есть время? Почему, по какой причине, человеку показалось, что время есть, хотя Махатмы говорят, что времени нет, Бог живёт вне времени?

**Арджуна**: Время - это показатель процесса.

**Рубик**: О! Время - это события, обуславливающие процессы. Все процессы есть майя, они нереальны, это ложь, иллюзия. При этом именно процессы обуславливают понятия расстояния, времени и массы. В отсутствии процесса нет ни времени, ни пространства, ни массы. Значит всё это - свойства иллюзии. Что же находится в основе процесса? Чему духовно должно соответствовать понятие процесса? Как назвать то, что, преломляясь через наше эго, воспринимается нами как процесс, но на самом деле таковым не является?

**Ананда**: Радостью.

**Рубик**: Блаженством! Итак, мы пришли к тому, что, если бы мы измеряли всё вокруг себя Блаженством, Светом и Любовью, то есть истинными физическими величинами, которые положены в основу мироздания - Сат-Чит-Ананда, и мы бы создали соответствующие формулы для вычислений, то у нас бы вся Вселенная преобразились и все наши понятия. Мы бы не смеялись над древними, считавшими, что Солнце вращается вокруг Земли. В современном сознании мироздание перевернуто. А что такое перевернутое мироздание? Это знак сатаны. То есть мы поместили своё сознание в сознание сатаны, мы пользуемся его категориями, его измерителями, и результаты, которые получаем, считаем истинными. Мир сатаны мы считаем истинным! Мы так и говорим: «наступит конец света»! Сама эта фраза предполагает, что в нашем мире есть свет, и когда он будет разрушен, это будет конец света. Не конец тьмы, неведения, не начало света, а его конец! Вот как!

Итак, когда вы положите в основу те величины, о которых я сказал, вы и сами увидите, что, действительно, Солнце вращается вокруг Земли. Вы спросите: а какая разница? В соответствие с миром относительности нет никакой разницы. Например, вы идете вокруг Земли или вы своими ногами толкаете её и она крутится, а вы стоите в одной точке? Какая разница, как эту модель себе представить, если обе работают и приносят одинаковые результаты? Если для вас нет разницы, это говорит о том, что ваш уровень сознания мироздания находится не выше анахаты - мира равенства, то есть равнодействия сил. А разница есть. Какая? Арджуна?

**Арджуна**: Первичен Бог. Поэтому всегда нужно исходить из того божественного, что есть в человеке, а не из человеческого. Поэтому коли Бог избрал себе местом воплощения Землю, а не Солнце, то соответственно Земля выше Солнца, и Солнце вращается вокруг неё.

**Рубик**: Это идея. Бог не может выбрать местом воплощения Землю, потому что Земля иллюзорна. Он может себе выбрать только то, что есть на самом деле, а кроме  Бога нет ничего. Можно сказать, что для Бога существуют духовные планеты - Вишнулока, Кришналока, Рамалока, и так далее. Их много и все они безвременны, то есть, они никогда не создавались и не разрушались. Это те пространственно-временные континуумы, которыми пользуются те или иные аватары Бога. Вот они - посмотрите на Шри янтру - вся вселенная, дом Бога.

Когда вы ставите всё на правильные места: первое - на первое, второе - на второе и так далее, то оказывается, что древние правы. Человек - столь великое явление, столь важное, значимое, что количество человеков, которые нас окружают, создают активность магнетическую, психическую, световую такой величины, что оно обнимает гарактики. Никакие Солнца не идут в сравнение. Если вы правильное значение положите в основу исчисления, если вы правильно будете относиться к тому, что меряете и к тому чем меряете. Я всякий раз говорю: вы получите тот результат, к которому готовы, который соответствует уровню вашего самосознания. Раздвиньте границы вашего самосознания. У вас уже есть книги, в которых это написано и просчитано. Вам в ваших школах преподавали эти книги с издевками, и вы сами над ними издевались: «Ха-ха! Они же глупые были! Какому идиоту могло в голову прийти, что Солнце вращается вокруг Земли? Ведь понятно, это же желтый карлик, конечно, все должны вокруг него вращаться».

Почему я так долго об этом говорю в связи с той цитатой, которой мы продолжили изучение Евангелия? Кто с кем встретился? Где встретился? Идите за мной и я вам покажу, где я живу! А!? Гениально! [^7] Где он живёт? Он никогда не покидал Своей обители! Просто в связи с изменением майи, вечный, бессмертный, абсолютный Бог предстал пред ними в лике Учителя на земле Палестинской. «Идите за мной» буквально означает: «Я всегда жил здесь на своей вечной абсолютной планете Блаженства, идите за мной и увидите, что это так».

Так. Дальше. 

**Ананда**: «Говорит им: пойдите и увидите. Они пошли и увидели, где Он живет; и пробыли у Него день тот. Было около десятого часа».

**Рубик**: И?

**Ананда**: Ну, а дальше уже всё: «Один из двух, слышавших от Иоанна об Иисусе и последовавших за Ним, был Андрей, брат Симона Петра».

**Рубик**: То есть Он принимает Мудрость, далее Он принимает Веру и в конце концов увенчивает это всё Любовью. Четыре точки складываются в объёмную фигуру, что называется пирамидой. Вот, мы посчитали все точки этой пирамиды, а где находится сам Иисус?

**Дурга**: На вершине?

**Рубик**: В центре. Почему? Нет большего или меньшего среди учеников, нет разницы между ними, а значит, то есть, речь идёт о правильной пирамиде, где все стороны - равносторонние треугольники. Если ты построишь биссектрисы, медианы, высоты - что угодно - в любом случае попадёшь в центр.

Почему добродетели упоминаются именно в этом порядке?

**Аджанта**: Может они в этом порядке просыпаются в процессе духовного роста?

**Рубик**: Правильно! Это постепенное открытие, как бы в каббале сказали, сефирот, лепестков. Как бутон лотоса, открывающий лепестки. Зачем растению нужны лепестки?

**Арджуна**: Для того, чтобы воспринимать свет, питаться светом.

**Рубик**: Нет. Для этого нужны листья, а я спрашиваю про лепестки.

**Аджанта**: Это результат…

**Рубик**: …единения со Светом, с Божественной Любовью, и результат процесса воспроизводства, то есть умножения этой Любви, построения нового, аналогичного себе. Это процесс умножения Блаженства, это вершина вершин, когда Бог сам в себе отражается бессчетное количество раз.

Понятно? Вопросы есть?

**Дурга**: Почему их четыре? Почему взяли именно этих?

**Рубик**: Потому что они - первичные. Это ступени приближения. На первом шаге - один,  Любовь, на втором шаге - ещё трое, Вера, Надежда и Мудрость, на третьем - двенадцать, на четвертом - семьдесят (После сего избрал Господь и других семьдесят [учеников], и послал их по два пред лицем Своим во всякий город и место, куда Сам хотел идти, и сказал им: жатвы много, а делателей мало; итак, молите Господина жатвы, чтобы выслал делателей на жатву Свою.), как дальше я не помню, но эти первые шаги очень хорошо известны: Вера, Надежа, Любовь и мать их София. 

Вспомните эпизод, где Иисус, взяв с собой Иоанна, Петра и Якова пошёл в дом Петра, где его тёща лежала в горячке (Мф. 8:14-15, Мк. 1:29-31, Лк. 4:38-39). Что такое горячка? Заражение крови, любая температура - это заражение крови: ангина, простуда, грипп. Что такое кровь? Душа! [«Только плоти с душею её, с кровью её, не ешьте». Быт. 9:4]. Иначе говоря, душа возмутилась, пришла в нестабильное состояние. Иисус пошёл и исцелил её, и она встала, и стала им всем прислуживать. Но зачем тратить столько времени, чернил и внимания на такие банальные вещи? Разве я, врач, придя в дом, где находится больной человек, не буду его лечить? Само собой разумеется! Если в твой дом пришли гости, даже если ты им ничем не обязан (а тут ещё и вылечили), разве ты не станешь им прислуживать? Зачем же этому посвящать целую страницу в Библии? Значит, на самом деле, эти строки важны и достойны более глубокого рассмотрения.

Нет сомнений в том, что у еврейского народа была мудрость: им была дарована Тора, у них были и Талмуд, и вся мудрость Египта. Вера у них тоже была: на каждом шагу - синагоги, в каждой из них - по свитку, и в субботу все обязательно туда приходили. У них была и Любовь и величайшее почтение к Богу. Но что они говорили? Что они согрешили пред Богом, что Он их оставил, но Он к ним придёт в лике Мессии. Как будет Мессия на иврите?

**Кешава**: Машиах.

**Рубик**: Правильно. А по-гречески - Христос. Вот они пришли в дом: Вера, Любовь и Мудрость, четвёртой точки не хватает - Надежды, которая лежала в горячке. Что 
это значит?  А то, что они жили в рабстве у римлян, они изнывали! Надежда «болела» у них. И вот пришёл Тот, Кого они ждали, Мессия, и Надежда исцелилась и стала им прислуживать, как говорится, «кормить всех надеждой». 

В Библии есть масса эпизодов, описывающих события, которые кажутся банальными, однако, если смотреть глубже, смотреть в суть, вы убедитесь, что каждое из этих упоминаний было очень важным.

[^1]: *Ра́джа-йо́га* («царская йога»), также известна как классическая йога — одна из шести ортодоксальных школ в философии индуизма, которая базируется на «Йога-сутрах» Патанджали. Основная цель раджа-йоги — контроль ума посредством медитации (дхьяна), осознание разницы между реальностью и иллюзией и достижение освобождения. (Раджа-йога // [Википедия](https://ru.wikipedia.org/wiki/Раджа-йога)).
[^2]: *Ка́рма-йо́га* («йога деятельности») также известна как бу́ддхи-йога. Ка́рма-йо́га основана на учении «Бхагавадгиты» — священного индуистского писания на санскрите, и её основной смысл состоит в выполнении предписанных обязанностей (дхармы) без привязанности к плодам труда. В результате такой деятельности становится возможным обретение мокши (спасения) или любви к Богу (бхакти). Это происходит посредством выполнения предписанных обязанностей без эгоистических мотивов с единственной целью удовлетворения Бога (Карма-йога // [Википедия](https://ru.wikipedia.org/wiki/Карма-йога)).
[^3]: *Джня́на-йо́га*, джнана-йога или гья́на-йо́га («йога мудрости») в адвайта-веданте совершенством джнана-йоги является осознание единства индивида, или в санскритской терминологии атмана, с безличным аспектом Абсолютной Истины — Брахманом. В адвайте джнана-йога выступает как первичный путь к достижению мокши. Это путь самоосознания, на котором индивид с помощью своего ума отличает реальность от иллюзии и осознаёт своё тождество с Брахманом. Полностью осознав это, индивид достигает стадии мокши (Джняна-йога // [Википедия](https://ru.wikipedia.org/wiki/Джняна-йога)).
[^4]: *Бха́кти-йо́га* («йога любви») — один из четырёх основных видов йоги в философии индуизма. Практика бхакти-йоги направлена на культивацию любви к Богу посредством бхакти — служения Богу с любовью и преданностью. Практика бхакти-йоги рекомендуется в ряде священных текстов индуизма как самый лёгкий и действенный вид йоги. Так, в «Бхагавад-гите» провозглашается её превосходство над остальными тремя основными видами йоги — карма-йогой, джнана-йогой и раджа-йогой (Бха́кти-йо́га // [Википедия](https://ru.wikipedia.org/wiki/Бхакти-йога)).
[^5]: *Пратьяхара* (санскр. — отведение) — отрешение и освобождение ума от власти чувств и их объектов с целью слияния с Абсолютом (Шри Юктешвар Гири, (2005) Святая Наука. М.: Сфера).
[^6]: *Майя* (санскр. — иллюзия, обман) — иллюзорность бытия как одно из ключевых понятий, с помощью которых осмысливается вселенная. Также космическая сила, делающая возможным феноменальное существование и, следовательно, восприятие. В адвайта-веданте — понятие, обозначающее все, что подвержено изменению, что имеет начало и конец (Шри Юктешвар Гири, (2005) Святая Наука. М.: Сфера).
[^7]: Иисус же, обратившись и увидев их идущих, говорит им: что вам надобно? Они сказали Ему: Равви́,- что значит: учитель,- где живешь? Говорит им: пойдите и увидите. Они пошли и увидели, где Он живет; и пробыли у Него день тот. Было около десятого часа (Ин. 1:38-39).