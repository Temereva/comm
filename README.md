
# Blog built with Gatsby, React, Markdown and Typescript.

## 🧐 What's inside:
Blog using two different post types, Markdown blog posts, featuring three types of navigation: 
- Mobile navigation / sidebar,
- Previous - Next on-page navigation for blog posts and articles,
- Pagination.

Project is being continuously deployed via Netlify.

<a href="https://read.satsanga.ru/">Demo website</a>
