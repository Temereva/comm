
const urlJoin = require ('url-join');
const config = require ('./data/siteConfig.ts'); 

module.exports = {
    pathPrefix: config.pathPrefix === "" ? "/" : config.pathPrefix,
    siteMetadata: {
        siteUrl: `https://read.satsanga.ru`, 
        title:  config.siteTitle,
        description: config.siteDescription,
        image_url: `${urlJoin(
            config.siteUrl,
            //config.pathPrefix
          )}/static/satsanga.png`,
          copyright: config.copyright,
    },
    plugins: [
        `gatsby-plugin-typescript`,
        'gatsby-plugin-react-helmet',
        {
            resolve: `gatsby-source-filesystem`,
            options: {
              path: `${__dirname}/src/pages/`,
              name: "src",
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
              path: `${__dirname}/src/comments/`,
              name: "src",
            },
          },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
              path: `${__dirname}/src/articles/`,
              name: "src",
            },
          },
          `gatsby-plugin-sharp`,
          {
            resolve: `gatsby-transformer-remark`,
            options: {
              plugins: [
                {
                  resolve: `gatsby-remark-images`,
                  options: {
                    // It's important to specify the maxWidth (in pixels) of
                    // the content container as this plugin uses this as the
                    // base for generating different widths of each image.
                    maxWidth: 920,
                    backgroundColor: `transparent`
                  },
                },
                {
                  resolve: "gatsby-remark-custom-blocks",
                  options: {
                    blocks: {
                      alignRight: {
                        classes: "alignRight",
                      },
                      alignLeft: {
                        classes: "alignLeft",
                      },
                      chapterNum: {
                        classes: "chapterNum",
                      },
                      imageBorder: {
                        classes: "imageBorder"
                      },
                      footNote: {
                        classes: "footNote"
                      }
                    },
                  },
                },
              ],
            },
          },
   /*       {
            resolve: `gatsby-plugin-google-gtag`,
            options: {
              // You can add multiple tracking ids and a pageview event will be fired for all of them.
              trackingIds: [
                config.GoogleAnalyticsId // Google Analytics / GA               
              ],
            },
          },*/
   /*        {resolve: `gatsby-plugin-google-analytics`,
          options: {
            // The property ID; the tracking code won't be generated without it
            trackingId: "UA-168815308-1",
            // Defines where to place the tracking script - `true` in the head and `false` in the body
            head: true,
            // Setting this parameter is optional
            anonymize: true,
            // Setting this parameter is also optional
            respectDNT: true,
            // Avoids sending pageview hits from custom paths
            //exclude: ["/preview/**", "/do-not-track/me/too/"],
            // Delays sending pageview hits on route update (in milliseconds)
            pageTransitionDelay: 0,
            // Enables Google Optimize using your container Id
            //optimizeId: "YOUR_GOOGLE_OPTIMIZE_TRACKING_ID",
            // Enables Google Optimize Experiment ID
            //experimentId: "YOUR_GOOGLE_EXPERIMENT_ID",
            // Set Variation ID. 0 for original 1,2,3....
            // variationId: "YOUR_GOOGLE_OPTIMIZE_VARIATION_ID",
            // Defers execution of google analytics script after page load
            defer: true,
            // Any additional optional fields
            sampleRate: 5,
            siteSpeedSampleRate: 10,
           // cookieDomain: "",
          },
        },*/
        {
            resolve: 'gatsby-plugin-sass',
            options: {
              cssLoaderOptions: {
                camelCase: true
              }
            }
        },

    ]
}
