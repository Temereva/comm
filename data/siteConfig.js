
var config = {
    siteTitle: "Комментарии к Святому Писанию",
    siteTitleAlt: "Комментарии к Святому Писанию",
    siteLogo: "satsanga.png",
    siteUrl: "/",
    fixedFooter: false,
    siteDescription: "Уникальное издание, посвященное постижению Святого Писания",
    dateFromFormat: "YYYY-MM-DD",
    dsteFormat: "DD/MM/YYYY",
    copyright: "© Общество Сатсанга",
    pathPrefix: "/comments"
};
// Make sure pathPrefix is empty if not needed
if (config.pathPrefix === "/") {
    config.pathPrefix = "";
}
else {
    // Make sure pathPrefix only contains the first forward slash
    config.pathPrefix = "" + config.pathPrefix.replace(/^\/|\/$/g, "");
}
// Make sure siteUrl doesn't have an ending forward slash
if (config.siteUrl.substr(-1) === "/") {
    config.siteUrl = config.siteUrl.slice(0, -1);
}
module.exports = config;
//# sourceMappingURL=siteConfig.js.map