const path = require("path")
const { createFilePath, createFileNode } = require(`gatsby-source-filesystem`)

exports.createPages = ({ graphql, actions }) => {

    const { createPage } = actions
  
    return new Promise ((resolve, reject) => {
      
      const commentPage = path.resolve(`src/templates/CommentTemplate.tsx`);
      const commentList = path.resolve(`src/templates/PostListTemplate.tsx`);

      //const articlePage = path.resolve(`src/templates/CommentTemplate.tsx`)

      resolve(
        graphql(`
        {
          allMarkdownRemark(
            sort: { order: ASC, fields: [frontmatter___quote] }
            limit: 1000
          ) {
              edges {
                node {              
                  fields{
                    slug
                  }
                html
                  frontmatter {
                    title
                    excerpt
                    date
                    tags
                    quote
                    posttype
                    chapter
                    verses
                  }
                }
              }
            }
        }
      `).then(result => {
            if (result.errors) {
              console.log(result.errors);
              return reject(result.errors);
            }
            
            const posts = result.data.allMarkdownRemark.edges;
            const articles = [];
            const comments = [];
            const postsPerPage = 6;
            const numPages = Math.ceil(posts.length / postsPerPage);
            const episodes = {};
  
            //
            // create post-list pages
            //
            Array.from({length: numPages}).forEach((_, i) => {
                createPage({
                  path: i === 0? `/commentary` : `/commentary/${i + 1}`,
                  component: commentList,
                  context:{
                    limit:postsPerPage,
                    skip: i * postsPerPage,
                    numPages,
                    currentPage: i + 1,
                  }
                });
            });

            //
            //create pages for each markdown file  
            //
            posts.forEach(({ node }, i) => {

              if (node.frontmatter.posttype === 'comment') {
                
                comments.push(posts[i]).node;
                
                //
                // build a table of contents to navigate between posts
                //
                if(node.frontmatter.chapter !== "") {
                  if (episodes.hasOwnProperty(node.frontmatter.chapter)) {
                    let versesLength = episodes[node.frontmatter.chapter].length;
                    episodes[node.frontmatter.chapter][versesLength] = node.frontmatter.verses;
                  } else {
                    episodes[node.frontmatter.chapter] = [];
                    episodes[node.frontmatter.chapter][0] = node.frontmatter.verses;  
                  }   
                }
              } else if (node.frontmatter.posttype === 'article') {
                articles.push(posts[i]).node; 
              } 
  
            });
            console.log(episodes);
            comments.forEach( ({ node}, i) => {
              const prev = i === 0 ? null : comments[i -1].node;
              const next = i === comments.length - 1 ? null : comments[i + 1].node;

              createPage({
                path: node.fields.slug,              
                component: commentPage,
                context: {
                  slug: node.fields.slug,
                  prev,
                  next,
                }, 
                // additional data can be passed via context
              } )
            }) 
            articles.forEach( ({ node}, i) => {
              const prev = i === 0 ? null : articles[i -1].node
              const next = i === articles.length - 1 ? null : articles[i + 1].node

              createPage({
                path: node.fields.slug,              
                component: commentPage,
                context: {
                  slug: node.fields.slug,
                  prev,
                  next,
                }, 
                // additional data can be passed via context
              } )
            }) 
              
          })
        )
      })
    }
      
exports.onCreateNode = ({ node, getNode, actions }) => {
    const { createNodeField } = actions
    if (node.internal.type === "MarkdownRemark") {
      
        const slug = createFilePath({ node, getNode, basePath: `pages` });
  
        createNodeField({
          node,
          name: "slug",
          value: slug,
        })
    }
   
  }
  exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
    if (stage === "build-html") {
      actions.setWebpackConfig({
        module: {
          rules: [
            {
              test: /bad-module/,
              use: loaders.null(),
            },
          ],
        },
      })
    }
  }
  